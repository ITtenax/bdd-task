package com.cucumber.core;

import ru.yandex.qatools.properties.PropertyLoader;
import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;

/**
 * User: Valentyn_Kvasov
 * Date: 26.10.2015
 * Time: 20:51
 */
@Resource.Classpath("testing.properties")
public class Properties {
    private static final Properties instance = new Properties();

    private Properties(){
        PropertyLoader.populate(this);
    }

    @Property("homepage.url")
    private String homepageUrl;

    @Property("implicity.wait")
    private String implicityWait;

    public static Properties getInstance() {
        return instance;
    }

    public String getImplicityWait() {
        return implicityWait;
    }

    public String getHomepageUrl() {
        return homepageUrl;
    }
}
