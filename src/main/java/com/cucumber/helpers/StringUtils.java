package com.cucumber.helpers;

import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Valentyn on 10/27/2015.
 */
public class StringUtils {
    public static String getStringByRegExp(String target,String regExp){
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(target);
        if(matcher.find()){
            return matcher.group();
        }
        throw new NoSuchElementException();
    }
}
