package com.cucumber.pages.pages;

import com.cucumber.pages.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * User: Valentyn_Kvasov
 * Date: 26.10.2015
 * Time: 19:16
 */
public class SearchResultPage extends Page {

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public static final By NEXT_PAGE_BUTTON_BY = By.xpath("//li[@class='pager-next']/a");
    public static final By SEARCH_RESULT_INPUT_BY = By.xpath("//input[@id='edit-name-1']");
    public static final By PRICE_SORTED_LINK_BY = By.xpath("//div[@class='order']//a[text()='цена']");
    public static final By SELECTED_PRODUCER_BY = By.xpath("//a[normalize-space(@class)='show_common_producer']/" +
            "preceding-sibling::a[contains(@class,'selected')]");
    public static final By SELECTED_MIN_PRICE_BY = By.xpath("//div[contains(text(),'Минимальная цена')]/" +
            "following-sibling::div//a[contains(@class,'selected')]");
    public static final By SELECTED_MAX_PRICE_BY = By.xpath("//div[contains(text(),'Максимальная цена')]/" +
            "following-sibling::div//a[contains(@class,'selected')]");
    public static final By SHOW_MORE_PRODUCER = By.xpath("//a[normalize-space(@class)='show_common_producer']");

    public static final String MIN_PRICE_LABELS_XPATH = "//div[contains(text(),'Минимальная цена')]/" +
            "following-sibling::div//a";
    public static final String MAX_PRICE_LABELS_XPATH = "//div[contains(text(),'Максимальная цена')]/" +
            "following-sibling::div//a";
    public static final String SEARCH_RESULTS_ITEM_PRICE_XPATH = "//div[@class='price']/strong";
    public static final String SEARCH_RESULTS_ITEM_NAME_XPATH = "//div[@class='name']/a";
    public static final String SEARCH_RESULTS_ITEMS_XPATH = "//div[@class='item']";
    public static final String PRODUCER_LABELS_XPATH = "//a[normalize-space(@class)='show_common_producer']/" +
            "preceding-sibling::a";
    public static final String PRODUCER_RESULT_COUTN_X = "//div[contains(text(),'Производитель:')]/" +
            "following-sibling::div//a[text()='%s']/following-sibling::i[1]";

    public static final String MIN_PRICE_X = "//div[contains(text(),'Минимальная цена')]/following-sibling::div//a[text()='%s']";
    public static final String MAX_PRICE_X = "//div[contains(text(),'Максимальная цена')]/following-sibling::div//a[text()='%s']";


    @FindBy(xpath = MIN_PRICE_LABELS_XPATH)
    List<WebElement> minPriceLabels;

    public List<WebElement> getMinPrice(){
        return minPriceLabels;
    }

    @FindBy(xpath = MAX_PRICE_LABELS_XPATH)
    List<WebElement> maxPriceLabels;

    public List<WebElement> getMaxPrice(){
        return maxPriceLabels;
    }

    @FindBy(xpath = SEARCH_RESULTS_ITEM_PRICE_XPATH)
    List<WebElement> searchResultsItemPrice;

    public List<WebElement> getSearchResultPrice(){
        return searchResultsItemPrice;
    }

    @FindBy(xpath = SEARCH_RESULTS_ITEM_NAME_XPATH)
    List<WebElement> searchResultsItemName;

    public List<WebElement> getSearchResultName(){
        return searchResultsItemName;
    }

    @FindBy(xpath = PRODUCER_LABELS_XPATH)
    List<WebElement> producerLabels;

    public List<WebElement> getSearchResultProducers(){
        return producerLabels;
    }

    @FindBy(xpath = SEARCH_RESULTS_ITEMS_XPATH)
    List<WebElement> searchResultsItems;

    public List<WebElement> getProductItems(){
        return searchResultsItems;
    }

}
