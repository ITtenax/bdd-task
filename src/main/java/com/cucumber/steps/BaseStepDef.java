package com.cucumber.steps;

import org.openqa.selenium.WebDriver;

import static com.cucumber.core.WebDriverFactory.Browser.Chrome;
import static com.cucumber.core.WebDriverFactory.createWebDriver;

/**
 * User: Valentyn_Kvasov
 * Date: 27.10.2015
 * Time: 19:27
 */
public class BaseStepDef {
    protected static WebDriver driver = createWebDriver(Chrome);
}
