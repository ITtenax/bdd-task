package com.cucumber;

import com.cucumber.core.WebDriverFactory;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * User: Valentyn_Kvasov
 * Date: 27.10.2015
 * Time: 21:21
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/main/java/com/features")
public class RunnerBDDTests {
    @AfterClass
    public static void setUp(){
        WebDriverFactory.closeDriver();
    }
}
