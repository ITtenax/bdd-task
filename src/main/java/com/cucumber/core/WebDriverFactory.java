package com.cucumber.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * User: Valentyn_Kvasov
 * Date: 27.10.2015
 * Time: 19:55
 */
public class WebDriverFactory {

    private static WebDriver driver;

    public static WebDriver createWebDriver(Browser browserType) {
        switch (browserType) {
            case Chrome:
                return driver = new ChromeDriver();
            case InternetExplorer:
                return driver = new InternetExplorerDriver();
            default:
                return driver = new FirefoxDriver();
        }
    }

    public static void closeDriver() {
        if (driver != null) {
            driver.close();
        }
    }

    public enum Browser {
        InternetExplorer, Chrome, Firefox
    }
}
