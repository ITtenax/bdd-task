package com.cucumber.steps.pagesteps;

import com.cucumber.pages.pages.HomePage;
import com.cucumber.steps.BaseStepDef;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

import static com.cucumber.core.Properties.getInstance;
import static com.cucumber.helpers.ClickUtils.click;
import static com.cucumber.helpers.GeneralUtils.getRandomNumber;
import static com.cucumber.pages.pages.HomePage.HOME_PAGE_CATEGORY_X;
import static com.cucumber.pages.pages.HomePage.HOME_PAGE_SUB_CATEGORY_X;
import static java.lang.Long.parseLong;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * User: Valentyn_Kvasov Date: 27.10.2015 Time: 19:55
 */
public class HomePageSteps extends BaseStepDef {

    private HomePage homePage = new HomePage(driver);

    @Given("^I open pn.com.ua$")
    public void openHomePage() {
        driver.get(getInstance().getHomepageUrl());
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(parseLong(getInstance().getImplicityWait()), SECONDS);
    }

    @When("^I select random category$")
    public void selectRandomCategory() {
        List<WebElement> homePageCategories = homePage.getHomePageCategories();
        click(driver, homePageCategories.get(getRandomNumber(homePageCategories.size())));
    }

    @And("^I select subcategory$")
    public void selectRandomSubCategory() {
        List<WebElement> homePageSubCategories = homePage.getHomePageSubCategories();
        click(driver, homePageSubCategories.get(getRandomNumber(homePageSubCategories.size())));
    }

    @When("^I select category (\\W+)$")
    public void selectCategory(String category) {
        click(driver, By.xpath(String.format(HOME_PAGE_CATEGORY_X, category)));
    }

    @When("^I select subcategory (\\W+)$")
    public void selectSubCategory(String subCategory) {
        click(driver, By.xpath(String.format(HOME_PAGE_SUB_CATEGORY_X, subCategory)));
    }
}
