package com.cucumber.pages.pages;

import com.cucumber.pages.Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * User: Valentyn_Kvasov
 * Date: 26.10.2015
 * Time: 19:26
 */
public class HomePage extends Page {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public static final String HOME_PAGE_CATEGORY_XPATH = "//a[@class='main_page_link_catalog']";
    public static final String HOME_PAGE_SUB_CATEGORY_XPATH = "//div[@class='column-content category']" +
            "//div[contains(@class,'warn')]//a";

    public static final String HOME_PAGE_CATEGORY_X = "//a[@class='main_page_link_catalog' and text()='%s']";
    public static final String HOME_PAGE_SUB_CATEGORY_X = "//div[@class='column-content category']" +
            "//div[contains(@class,'warn')]//a[text()='%s']";

    @FindBy(xpath = HOME_PAGE_CATEGORY_XPATH)
    List<WebElement> homePageCategory;

    @FindBy(xpath = HOME_PAGE_SUB_CATEGORY_XPATH)
    List<WebElement> homePageSubCategory;


    public List<WebElement> getHomePageCategories(){
        return homePageCategory;
    }

    public List<WebElement> getHomePageSubCategories(){
        return homePageSubCategory;
    }

}
