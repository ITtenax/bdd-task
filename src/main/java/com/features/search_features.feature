Feature: Search filters

  @Scenario1
  Scenario: Verify filter by price
    Given I open pn.com.ua
    When I select random category
    And I select subcategory
    And I set minimal price and maximal price
    Then Verify that results are correct

  @Scenario2
  Scenario: Verify filter by Producer
    Given I open pn.com.ua
    When I select random category
    And I select subcategory
    And I select random producer
    Then Verify that count of goods into search result are equal for count of producer name
    And Verify that, goods name into search result start with for selected

  @Scenario3
  Scenario: Verify search by Name
    Given I open pn.com.ua
    When I select random category
    And I select subcategory
    And I sort items by price
    And I Take the name of the cheapest item enter the name in the search box
    Then Check that the search result is equal to 1
    And Check that the name of the product is equal to the specified name

  @Scenario4
  Scenario: Verify filter by price2
    Given I open pn.com.ua
    When I select category Компьютеры
    And I select subcategory Мониторы
    And I set minimal price 5000 and maximal price 9000
    Then Verify that results are correct