package com.cucumber.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.cucumber.core.Properties.getInstance;
import static java.lang.Long.parseLong;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by Valentyn on 10/27/2015.
 */
public class WaitUtils {
    public static boolean waitForCondition(WebDriver driver, ExpectedCondition<?> condition) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        try {
            wait.until(condition);
        } catch (NoSuchElementException | TimeoutException | ElementNotVisibleException e) {
            return false;
        }finally {
            driver.manage().timeouts().implicitlyWait(parseLong(getInstance().getImplicityWait()), SECONDS);
        }
        return true;
    }

    public static WebElement waitForElementVisibility(WebDriver driver, By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        try {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
        } finally {
            driver.manage().timeouts().implicitlyWait(parseLong(getInstance().getImplicityWait()), SECONDS);
        }
        return driver.findElement(locator);
    }
}
