package com.cucumber.steps.pagesteps;

import com.cucumber.pages.pages.SearchResultPage;
import com.cucumber.steps.BaseStepDef;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import static com.cucumber.helpers.ClickUtils.click;
import static com.cucumber.helpers.ClickUtils.typeTo;
import static com.cucumber.helpers.GeneralUtils.getRandomNumber;
import static com.cucumber.helpers.StringUtils.getStringByRegExp;
import static com.cucumber.helpers.WaitUtils.waitForCondition;
import static com.cucumber.helpers.WaitUtils.waitForElementVisibility;
import static com.cucumber.pages.pages.SearchResultPage.MIN_PRICE_X;
import static com.cucumber.pages.pages.SearchResultPage.NEXT_PAGE_BUTTON_BY;
import static com.cucumber.pages.pages.SearchResultPage.PRICE_SORTED_LINK_BY;
import static com.cucumber.pages.pages.SearchResultPage.PRODUCER_RESULT_COUTN_X;
import static com.cucumber.pages.pages.SearchResultPage.SEARCH_RESULT_INPUT_BY;
import static com.cucumber.pages.pages.SearchResultPage.SELECTED_MAX_PRICE_BY;
import static com.cucumber.pages.pages.SearchResultPage.SELECTED_MIN_PRICE_BY;
import static com.cucumber.pages.pages.SearchResultPage.SELECTED_PRODUCER_BY;
import static com.cucumber.pages.pages.SearchResultPage.SHOW_MORE_PRODUCER;
import static com.google.common.collect.Collections2.transform;
import static java.lang.Integer.parseInt;
import static org.apache.commons.lang3.StringUtils.deleteWhitespace;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.startsWith;
import static org.openqa.selenium.Keys.ENTER;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfAllElements;

/**
 * User: Valentyn_Kvasov Date: 27.10.2015 Time: 20:01
 */
public class SearchResultSteps extends BaseStepDef {
    private SearchResultPage searchResultPage = new SearchResultPage(driver);

    @And("^I set minimal price and maximal price$")
    public void setMaximalMinimalPrice() {
        click(driver, searchResultPage.getMinPrice().get(0));
        click(driver, searchResultPage.getMaxPrice().get(getRandomNumber(searchResultPage.getMaxPrice().size())));
    }

    @Then("^Verify that results are correct$")
    public void verifySearchResultPriceIsCorrect() {
        List<Integer> priceList = new ArrayList<>();
        Integer minPrice = parseInt(waitForElementVisibility(driver, SELECTED_MIN_PRICE_BY).getText());
        Integer maxPrice = parseInt(waitForElementVisibility(driver, SELECTED_MAX_PRICE_BY).getText());
        do {
            waitForCondition(driver, visibilityOfAllElements(searchResultPage.getSearchResultPrice()));
            priceList.addAll(transform(searchResultPage.getSearchResultPrice(),
                    input -> parseInt(getStringByRegExp(deleteWhitespace(input.getText()), "(\\d+)"))));
            click(driver, NEXT_PAGE_BUTTON_BY);
        } while (waitForCondition(driver, elementToBeClickable(NEXT_PAGE_BUTTON_BY)));
        priceList = priceList.stream().sorted().collect(Collectors.toList());
        assertThat(priceList.get(0), allOf(lessThanOrEqualTo(maxPrice), greaterThanOrEqualTo(minPrice)));
        assertThat(priceList.get(priceList.size() - 1), allOf(lessThanOrEqualTo(maxPrice),
                greaterThanOrEqualTo(minPrice)));
    }

    @And("^I select random producer$")
    public void selectRandomProducer() {
        List<WebElement> producerList = searchResultPage.getSearchResultProducers();
        click(driver, producerList.get(getRandomNumber(producerList.size())));
    }

    @Then("^Verify that count of goods into search result are equal for count of producer name$")
    public void verifyProducerCount() {
        WebElement selectedProducer = driver.findElement(SELECTED_PRODUCER_BY);
        click(driver, SHOW_MORE_PRODUCER);
        String producerCountUI = getStringByRegExp(driver.findElement(By.xpath(String.format(PRODUCER_RESULT_COUTN_X,
                selectedProducer.getText()))).getAttribute("textContent"), "(\\d+)");
        List<WebElement> productList = new ArrayList<>();
        do {
            productList.addAll(searchResultPage.getProductItems());
            click(driver, NEXT_PAGE_BUTTON_BY);
        } while (waitForCondition(driver, elementToBeClickable(NEXT_PAGE_BUTTON_BY)));
        assertThat(productList.size(), equalTo(new Integer(producerCountUI)));
    }

    @And("^Verify that, goods name into search result start with for selected$")
    public void verifyItemNames() {
        String producerName = driver.findElement(SELECTED_PRODUCER_BY).getText();
        List<String> productList = new ArrayList<>();
        do {
            productList.addAll(transform(searchResultPage.getSearchResultName(), WebElement::getText));
            click(driver, NEXT_PAGE_BUTTON_BY);
        } while (waitForCondition(driver, elementToBeClickable(NEXT_PAGE_BUTTON_BY)));
        productList.stream().forEach(productName -> assertThat(productName, startsWith(producerName)));
    }

    @And("^I sort items by price$")
    public void clickOnSortPriceLink() {
        click(driver, PRICE_SORTED_LINK_BY);
    }


    @And("^I Take the name of the cheapest item enter the name in the search box$")
    public void takeCheapestItemIntoSearchBox() {
        List<WebElement> searchResultName = searchResultPage.getSearchResultName();
        typeTo(driver, SEARCH_RESULT_INPUT_BY, searchResultName.get(0).getText());
        driver.findElement(SEARCH_RESULT_INPUT_BY).sendKeys(ENTER);
    }

    @Then("^Check that the search result is equal to (\\d+)$")
    public void checkSearchResultCount(int arg1) {
        assertThat(searchResultPage.getSearchResultName().size(), equalTo(arg1));
    }

    @And("^Check that the name of the product is equal to the specified name$")
    public void checkProductName() {
        String searchQuery = driver.findElement(SEARCH_RESULT_INPUT_BY).getAttribute("value");
        String searchResult = searchResultPage.getSearchResultName().get(0).getText();
        assertThat(searchQuery, equalTo(searchResult));
    }

    @And("^I set minimal price (\\d+) and maximal price (\\d+)$")
    public void selectMinMaxPrice(int minPrice, int maxPrice) throws Throwable {
        click(driver, By.xpath(String.format(MIN_PRICE_X, minPrice)));
        click(driver, By.xpath(String.format(SearchResultPage.MAX_PRICE_X, maxPrice)));
    }
}
