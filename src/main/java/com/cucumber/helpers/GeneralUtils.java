package com.cucumber.helpers;

import java.util.Random;

/**
 * Created by Valentyn on 10/27/2015.
 */
public class GeneralUtils {
    public static int getRandomNumber(int size){
        return new Random().nextInt(size - 1);
    }
}
