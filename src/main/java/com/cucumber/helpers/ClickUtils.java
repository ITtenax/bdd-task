package com.cucumber.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static com.cucumber.helpers.WaitUtils.waitForCondition;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElement;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfAllElementsLocatedBy;

/**
 * Created by Valentyn on 10/27/2015.
 */
public class ClickUtils {
    public static void click(WebDriver driver, By locator) {
        if(waitForCondition(driver, elementToBeClickable(locator))){
            driver.findElement(locator).click();
        }
    }

    public static void click(WebDriver driver, WebElement locator) {
        if(waitForCondition(driver, elementToBeClickable(locator))) {
            locator.click();
        }
    }

    public static void typeTo(WebDriver driver, By locator, String text) {
        waitForCondition(driver, visibilityOfAllElementsLocatedBy(locator));
        WebElement input = driver.findElement(locator);
        input.clear();
        input.sendKeys(text);
        waitForCondition(driver, textToBePresentInElement(input, text));
    }
}
